﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class Score {
    const string nameplayer = "nameplayer:";

    public static void Start() {
        //ResetHighscore();
        Bird.GetInstance().OnDied += Bird_OnDied;
    }

    private static void Bird_OnDied(object sender, System.EventArgs e) {
        TrySetNewHighscore(Level.GetInstance().GetPipesPassedCount());
    }

    public static int GetHighscore() {
        return PlayerPrefs.GetInt("highscore");
    }

    public static string GetNameHighscore()
    {
        return PlayerPrefs.GetString(nameplayer);
    }
    // Day nay 
    public static bool TrySetNewHighscore(int score) {
        int currentHighscore = GetHighscore();
        if (score > currentHighscore) {
            // New Highscore
            PlayerPrefs.SetInt("highscore", score);
            PlayerPrefs.SetString("nameplayer:", GameManager.Intance.NamePlayer);
            Debug.Log("highscore player:" + GameManager.Intance.NamePlayer);
            PlayerPrefs.Save();
            return true;
        } else {
            Debug.Log("highscore player:" + PlayerPrefs.GetString("nameplayer:"));
            return false;
        }
    }

    public static void ResetHighscore() {
        PlayerPrefs.SetInt("highscore", 0);
        PlayerPrefs.Save();
    }
}
