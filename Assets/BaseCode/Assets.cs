﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace BaseCode
{

   
    public class Assets : MonoBehaviour {

       
        private static Assets _i; 

       
        public static Assets i {
            get { 
                if (_i == null) _i = (Instantiate(Resources.Load("BaseCodeAssets")) as GameObject).GetComponent<Assets>(); 
                return _i; 
            }
        }
        public Sprite s_White;
        
    }

}